package com.zhengjiang.springboot.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.PageRequest;

import com.zhengjiang.springboot.demo.entity.UserInfo;
import com.zhengjiang.springboot.demo.respository.UserRepository;
import com.zhengjiang.springboot.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserInfo findById(Long id) {
		return userRepository.getOne(id);
	}
	@Override
	public List<UserInfo> getUserList() {
		return userRepository.findAll();
	}

	@Override
	public UserInfo getUserByName(String name) {
		UserInfo userInfo = userRepository.findByName(name);
		return userInfo;
	}

	@Override
	public UserInfo addUserInfo(UserInfo userInfo) {
		return userRepository.save(userInfo);
	}

	@Override
	public UserInfo updateUserInfoById(UserInfo userInfo) {
		return userRepository.save(userInfo);
	}

	@Override
	public void deleteUserInfoById(Long id) {
		userRepository.deleteById(id);
	}

	@Override
	public List<UserInfo> getCurrentUserList() {
		Sort sort=new Sort(Sort.Direction.DESC,"createTime");
		return userRepository.findAll(sort);
	}
	@Override
	public Page<UserInfo> getPageUserList() {
		Sort sort=new Sort(Sort.Direction.DESC,"createTime");
		PageRequest pageable=new PageRequest(0,5,sort);
		userRepository.findAll(pageable);
		return userRepository.findAll(pageable);
	}
}
