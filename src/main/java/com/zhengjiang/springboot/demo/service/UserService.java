package com.zhengjiang.springboot.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.zhengjiang.springboot.demo.entity.UserInfo;

public interface UserService {
	
	UserInfo findById(Long id);
	List<UserInfo> getUserList();
    UserInfo getUserByName(String name);
    UserInfo addUserInfo(UserInfo userInfo);
    UserInfo updateUserInfoById(UserInfo userInfo);
    void deleteUserInfoById(Long Id);
    List<UserInfo>getCurrentUserList();
    Page<UserInfo> getPageUserList();
}
