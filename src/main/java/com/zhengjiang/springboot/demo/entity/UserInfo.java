package com.zhengjiang.springboot.demo.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity
@Table(name = "t_sys_user")
@Component
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" })
public class UserInfo{
	
	public UserInfo() {}
	
	public UserInfo(String name) {this.name = name;}
	@Id
    @GeneratedValue
	private  Long id; //ID
    private  String name; //姓名
    private  String jobNumber; //工号
    private Date createTime; //创建时间
}
