package com.zhengjiang.springboot.demo.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.zhengjiang.springboot.demo.entity.UserInfo;

public interface UserRepository extends JpaRepository<UserInfo, Long>,JpaSpecificationExecutor<UserInfo> {
	UserInfo findByName(String name);
}
