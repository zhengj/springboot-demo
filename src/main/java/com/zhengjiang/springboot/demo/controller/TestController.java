package com.zhengjiang.springboot.demo.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.zhengjiang.springboot.demo.entity.UserInfo;
import com.zhengjiang.springboot.demo.service.UserService;

@RestController
@RequestMapping("/test")
public class TestController {
	  
		@Autowired
	    private UserService userService;
		
		/**
		 * 
		 * @param id
		 * @return
		 */
		@GetMapping(value = "/getOne")
		public UserInfo getOne(@RequestParam("id") Long id) {
			return userService.findById(id);
		}

	    /**
	     * 获取所有用户
	     * @return
	     */
	    @GetMapping(value = "/getUserList")
	    public List<UserInfo> getUserList() {
	        return userService.getUserList();
	    }

	    /**
	     * 根据用户名查找
	     * @param name
	     * @return
	     */
	    @GetMapping(value = "/getUserInfo")
	    public UserInfo getUserInfoByName(@RequestParam("name") String name) {
	    	UserInfo u = userService.getUserByName(name);
	        return u;
	    }

	    /**
	     * 根据createTime倒序查询
	     * @return
	     */
	    @GetMapping(value = "/getCurrentUserList")
	    public List<UserInfo> getCurrentUserList(){
	        return userService.getCurrentUserList();
	    }

	    /**
	     * 分页查找
	     * @return
	     */
	    @GetMapping(value="/getPageUserList")
	    public Page<UserInfo> getPageUserList(){
	        return userService.getPageUserList();
	    }

	    /**
	     * 添加用户
	     * @param userInfo
	     * @return
	     */
	    @PostMapping(value = "/addUserInfo")
	    public UserInfo addUserInfo(UserInfo userInfo) {
	        return userService.addUserInfo(userInfo);
	    }

	    /**
	     * 更新用户
	     * @param userInfo
	     * @return
	     */
	    @PostMapping(value ="/updateUserInfo")
	    public UserInfo updateUserInfo(UserInfo userInfo){
	        return userService.updateUserInfoById(userInfo);
	    }

	    /**
	     * 删除用户
	     * @param id
	     */
	    @PostMapping(value="/deleteUserInfo")
	    public void deleteUserInfo(@RequestParam("id") Long id){
	         userService.deleteUserInfoById(id);
	    }

	    @InitBinder
	    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
	        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));/*TimeZone时区，解决差8小时的问题*/
	        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	    }
}
