package com.zhengjiang.springboot.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zhengjiang.springboot.demo.entity.UserInfo;

@RestController
public class HelloWorldController {
	@RequestMapping("/hello")
	public UserInfo index() {
		return new UserInfo("lisi");
	}
}
