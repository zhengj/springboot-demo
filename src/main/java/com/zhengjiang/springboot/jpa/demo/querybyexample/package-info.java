/**
 * Sample showing Query-by-Example related features of Spring Data JPA.
 *
 * @author Mark Paluch
 */
package com.zhengjiang.springboot.jpa.demo.querybyexample;
