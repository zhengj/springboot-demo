/**
 * Package showing auditing support with Spring Data repositories.
 */
package com.zhengjiang.springboot.jpa.demo.auditing;